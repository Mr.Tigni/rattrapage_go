import (
	"fmt"
	"github.com/spf13/cobra"
)

var helpCmd = &cobra.Command{
	Use:   "h",
	Short: "Affiche la syntaxe, c’est à dire le nom du programme compilé et la liste des
	commandes disponibles.",
	Long:  `Affiche la syntaxe, c’est à dire le nom du programme compilé et la liste des
	commandes disponibles`,
}