import (
	"fmt"
	"github.com/spf13/cobra"
)

var serveCmd = &cobra.Command{
	Use:   "config",
	Short: "Lance le serveur wiki.",
	Long:  `Lance le serveur wiki.`,
}